from random import *
from string import ascii_lowercase
from numpy import sqrt
import argparse
import random
import os


def norm(x):
    l=""
    for y in str(x):
        if y==".":
            l=l+","
        else:
            l=l+y
    return l

def generar(n,fun):
    l=generar_lista(9,9)
    l=mezclar_lista(l)
    file = open("./filename.txt", "w")
    file.write('\\begin{multicols}{3} '+ os.linesep)
    file.write('\\begin{enumerate}[(1)]\itemsep35pt '+ os.linesep)
    i=0    
    while i<n:
        file.write('\\item '+fun(l[i])+ os.linesep)
        i=i+1
    file.write('\\end{enumerate} '+ os.linesep)
    file.write('\\end{multicols} '+ os.linesep)
    file.close()
    return 0

def generar_lista(a,b):
    l=[]    
    for i in range(2,a+2):
        for j in range(2,b+2):            
            l=l+[[i,j]]       
    return l

def mezclar_lista(l):
    k=[]
    copia=l
    n=len(l)
    while len(k)<n:
        i=randint(0,len(copia)-1)
        k=k+[copia[i]]
        copia=copia[:i]+copia[i+1:]
    return k

def logaritmo(par):#generar el string, linea de jercicio
    b=par[1]
    c=par[0]
    a=b**c
    b=str(b)
    a=str(a)
    c=str(c)
    #l="$\\log_{"+b+"}{"+a+"}="+str(c)+"$"
    l="$\\log_{"+b+"}{"+a+"}=$"
    return l

def polinomica(par):
    i=random.choice(ascii_lowercase)
    a=random.randint(-10, 10)
    b=random.randint(-10, 10)
    c=random.randint(-10, 10)
    if b**2-4*a*c>0:
        x1 = (-b+sqrt(b**2-4*a*c))/(2*a)
        x2 = (-b-sqrt(b**2-4*a*c))/(2*a)
        v = -b/2*a
        v2 = a*v**2+b*v+c
        if x1.is_integer() and x2.is_integer() and v.is_integer() and v2.is_integer():
            if c == 0 and b != 0:
                b=str('%+d' %b)
                a=str('%+d' %a)
                l="$"+i+"(x)="+a+"{x}^{2}{"+b+"x}$"
            elif b == 0 and c != 0:
                a=str('%+d' %a)
                c=str('%+d' %c)
                l="$"+i+"(x)="+a+"{x}^{2}{"+c+"}$"
            else:
                b=str('%+d' %b)
                a=str('%+d' %a)
                c=str('%+d' %c)
                l="$"+i+"(x)="+a+"{x}^{2}{"+b+"x}{"+c+"}$"
            return l
        else:
            return polinomica(par)
    else:
        return polinomica(par)



FUNCIONES = {'polinomica' : polinomica,
             'logaritmo' : logaritmo}

parser = argparse.ArgumentParser(description='Imprime ejercicios a mansalva')
parser.add_argument('entero', metavar='N', type=int, help='debe ingresar el numero de ejercicios que desea')
parser.add_argument('ejercicio', choices = FUNCIONES.keys())
args = parser.parse_args()
func = FUNCIONES[args.ejercicio]
copias = args.entero
generar(copias, func)



#generar(80, polinomica)
## insertar en el .tex
## \input{filename.txt} 

comando1="pdflatex -jobname "+args.ejercicio+" -synctex=1 -interaction=nonstopmode template.tex"
#os.system('pdflatex -synctex=1 -interaction=nonstopmode basico.tex')
os.system(comando1)
os.system('rm *.log')
os.system('rm *.gz')
os.system('rm *.aux')
#comando2="atril "+titulo_documento +".pdf & "
#os.system('atril basico.pdf & ')
print(args.ejercicio) 
