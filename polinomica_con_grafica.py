from random import randint, choice
from string import ascii_lowercase
from numpy import sqrt
import argparse
import random
import os
import matplotlib.pyplot as plt
import numpy as np

def generar(n, fun):
    l = mezclar_lista(generar_lista(9, 9))
    with open("./filename.txt", "w") as file:
        file.write('\\begin{multicols}{3}\n')
        file.write('\\begin{enumerate}[(1)]\\itemsep35pt\n')
        for i in range(n):
            file.write(f'\\item {fun(l[i])}\n')
        file.write('\\end{enumerate}\n')
        file.write('\\end{multicols}\n')

def generar_lista(a, b):
    return [[i, j] for i in range(2, a+2) for j in range(2, b+2)]

def mezclar_lista(l):
    random.shuffle(l)
    return l

def graficar_polinomica(a, b, c, nombre_archivo):
    x = np.linspace(-10, 10, 400)
    y = a * x**2 + b * x + c
    plt.figure()
    plt.plot(x, y, label=f"${a}x^2 {b:+}x {c:+}$")
    plt.axhline(0, color='black', linewidth=0.5)
    plt.axvline(0, color='black', linewidth=0.5)
    plt.grid(True, which='both')
    plt.title('Gráfica de la función polinómica')
    plt.xlabel('x')
    plt.ylabel('f(x)')
    plt.legend()
    plt.savefig(f"{nombre_archivo}.png")
    plt.close()

def polinomica_con_grafica(par):
    i = choice(ascii_lowercase)
    while True:
        a, b, c = randint(-10, 10), randint(-10, 10), randint(-10, 10)
        discriminante = b**2 - 4*a*c
        
        if a != 0 and discriminante >= 0:
            x1 = (-b + sqrt(discriminante)) / (2 * a)
            x2 = (-b - sqrt(discriminante)) / (2 * a)
            v = -b / (2 * a)
            v2 = a * v**2 + b * v + c
            
            if all(map(lambda x: x.is_integer(), [x1, x2, v, v2])):
                terms = []
                if a != 0:
                    a_str = f"{'' if a == 1 else '-' if a == -1 else a}x^2"
                    terms.append(a_str)
                if b != 0:
                    b_str = f"{'+' if b > 0 else ''}{'' if b == 1 else '-' if b == -1 else b}x"
                    terms.append(b_str)
                if c != 0:
                    c_str = f"{'+' if c > 0 else ''}{c}"
                    terms.append(c_str)
                
                if terms:
                    nombre_archivo = f"grafica_{i}"
                    graficar_polinomica(a, b, c, nombre_archivo)
                    return f"\\includegraphics[width=0.45\\textwidth]{{{nombre_archivo}.png}}"

def logaritmo(par):
    b, c = str(par[1]), str(par[0])
    a = str(par[1] ** par[0])
    return f"$\\log_{{{b}}}{{{a}}}=$"

FUNCIONES = {'polinomica': polinomica_con_grafica, 'logaritmo': logaritmo}

parser = argparse.ArgumentParser(description='Imprime ejercicios a mansalva')
parser.add_argument('entero', metavar='N', type=int, help='debe ingresar el numero de ejercicios que desea')
parser.add_argument('ejercicio', choices=FUNCIONES.keys())
args = parser.parse_args()

generar(args.entero, FUNCIONES[args.ejercicio])

comando1 = f"pdflatex -jobname={args.ejercicio} -synctex=1 -interaction=nonstopmode template.tex"
os.system(comando1)
os.system('rm *.log *.gz *.aux')

print(args.ejercicio)
